package android.rxjavadragger.service;

import android.rxjavadragger.model.FlowerResponse;

import java.util.List;

import rx.Observable;

/**
 * Created by Supriya A on 8/8/2017.
 */

public interface FlowerViewInterface {

    void onCompleted();

    void onError(String message);

    void onFlowers(List<FlowerResponse> flowerResponses);

    Observable<List<FlowerResponse>> getFlowers();
}
