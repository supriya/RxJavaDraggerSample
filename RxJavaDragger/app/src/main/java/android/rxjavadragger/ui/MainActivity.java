package android.rxjavadragger.ui;

import android.rxjavadragger.R;
import android.rxjavadragger.application.MainApplication;
import android.rxjavadragger.base.FlowerPresenter;
import android.rxjavadragger.model.FlowerResponse;
import android.rxjavadragger.service.FlowerService;
import android.rxjavadragger.service.FlowerViewInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;

public class MainActivity extends AppCompatActivity implements FlowerViewInterface {

    @Inject
    FlowerService mService;

    FlowerPresenter mFlowerPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ((MainApplication)getApplication()).getApiComponent()
                .inject(MainActivity.this);

        mFlowerPresenter = new FlowerPresenter(MainActivity.this);
        mFlowerPresenter.onCreate();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mFlowerPresenter.onResume();
        mFlowerPresenter.fetchFlowers();
    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onFlowers(List<FlowerResponse> flowerResponses) {

        Log.d("response","On response ---" +flowerResponses.size());
    }

    @Override
    public Observable<List<FlowerResponse>> getFlowers() {
        return mService.getFlowerList();
    }
}
