package android.rxjavadragger.base;

import android.rxjavadragger.model.FlowerResponse;
import android.rxjavadragger.service.FlowerViewInterface;

import java.util.List;

import rx.Observer;

/**
 * Created by Supriya A on 8/8/2017.
 */

public class FlowerPresenter extends BasePresenter implements Observer<List<FlowerResponse>> {

    private FlowerViewInterface mViewInterface;

    public FlowerPresenter(FlowerViewInterface viewInterface) {
        mViewInterface = viewInterface;
    }

    @Override
    public void onCompleted() {
        mViewInterface.onCompleted();
    }

    @Override
    public void onError(Throwable e) {
        mViewInterface.onError(e.getMessage());
    }

    @Override
    public void onNext(List<FlowerResponse> flowerResponses) {
        mViewInterface.onFlowers(flowerResponses);
    }

    public void fetchFlowers() {
        unSubscribeAll();
        subscribe(mViewInterface.getFlowers(),FlowerPresenter.this);
    }
}
